#!/bin/bash
export CROSS_COMPILE=/home/hades/Desktop/kernel/hK-home/toolchains/aarch64-linux-android-4.9/bin/aarch64-linux-android-
make clean
make mrproper
make ARCH=arm64 distclean
mkdir -p hK-out/out

aver=Oreo
hver=v1.0
hdev=A520F

export ARCH=arm64
export CROSS_COMPILE=/home/hades/Desktop/kernel/hK-home/toolchains/aarch64-linux-android-4.9/bin/aarch64-linux-android-
export LOCALVERSION=-hadesKernel_$hver-$hdev-$aver

make ARCH=arm64 a5y17lte_00_defconfig
make ARCH=arm64 -j128

cp arch/arm64/boot/Image $(pwd)/hK-out/out/zImage
cp arch/arm64/boot/dtb.img $(pwd)/hK-out/out/dts

echo "Building ramdisk structure..."
cd $(pwd)/hK-ramdisk/A520F/
find | fakeroot cpio -o -H newc | gzip -9 > ../../hK-out/out/hK-ramdisk.gz
cd ../../



mkbootimg --kernel $(pwd)/hK-out/out/zImage \
				--ramdisk $(pwd)/hK-out/out/hK-ramdisk.gz \
				--base 10000000 \
				--board SRPPH19B001KU \
				--pagesize 2048 \
				--pagesize 2048 \
				--kernel_offset 00008000 \
				--ramdisk_offset 01000000 \
				--second_offset 00f00000 \
				--tags_offset 00000100 \
				--os_version 8.0.0 \
				--os_patch_level 2018-04 \
				--dt $(pwd)/hK-out/out/dts \
				--output $(pwd)/hK-out/hadesKernel-$hver-$hdev-$aver.img

echo -n "SEANDROIDENFORCE" >> $(pwd)/hK-out/hadesKernel-$hver-$hdev-$aver.img

echo "Done..."



